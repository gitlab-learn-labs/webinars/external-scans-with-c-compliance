# Example Project for external scanning on C/C++ 

This project provides a working example of C/C++ scanning utilizing a combination of GitLab scanners as well as external scanners. It was forked from the GitLab [project](https://gitlab.com/gitlab-org/security-products/demos/coverage-fuzzing/c-cpp-fuzzing-example) providing an example for LibFuzzer scanner. As the LibFuzzer is pretty specific I felt this was a good launch point from where other scanners can be added.

This project also makes use of compliance pipeline and should be presented along with the *External Required Scan* project, simulating the required scan executions as required by both security and compliance teams. 

## The Story

This project demonstrates the example, utilizing C/C++ as the language of choice, where the development team has chosen their own scanners, but the security team also wants their specific scanners to be run. The LibFuzzer scanner is enabled locally as the scanner configuration is very specific to the code libraries being tested. The developer also enabled some of the GitLab native scanners to give them some awareness of issues that may be in their code. Historically they've had to wait for the security team to scan the repositories. Now they want to be preemptive to catch and fix issues as they develop code, not days or weeks later. 

The security teams, however, still have their own tests that need to be run in order to maintain compliance and corporate policy. The Development team is responsible for addressing any issues raised by the security scan. Any issue to be addressed needs to be copied from the scan results, and entered into the related issue tracking system. When the code is submitted for approval, the approval package must include the results of the security scans executed on the specific branch and latest commmit submitted for approval.

### Challenges

There are a few challenges with the way things operate currently.
* There isn't a tight correlation between the scan results and the branch and commit
* Sometimes issues aren't copied over accurately, or not copied over at all
* Developers require access multiple tools to obtain a complete list of known issues
* There is no standard scan execution policy requiring security scans

The challenges open up the team to the following...
* Scan results submitted for approval may not include the most recent results associated with the branch => Vulnerable to releasing issues
* Some issues may not be copied over to the project, yielding a late stage discovery of issues to be addressed => Delays production release
* Security related issues are incomplete - not all information is getting copied over => Reduces time to resolution
* Multiple tools with multiple interfaces => Increasing distraction and switch-tasking

## How to use this Project

There are two parts to this project, the core code and example, as well as the compliance framework connection defining the external mandatory scans. Within the compliance framework, the security team is able to define whatever mandatory scans they require, with the results either directly integrated into GitLab or provided as an artifact. The latter is not as useful, but work must be done by some party to ensure compatability (perhaps a JSON converter for the horusec scan). Run the pipeline prior to your demonstration to ensure that issues are populated in the vulnerability report. 

### About the External Scans

The external Semgrep scan utilizes a token that is hardcoded.  This allows the project to interface with the free Semgrep scanner hosted on https://semgrep.dev. If the job fails you may need to obtain your own token. Unfortunately IDs between the GitLab SAST and the external Semgrep scans are replicated, so there isn't much differentiation within the report. Ideally, eventually we'll have additional scanners that are defined uniquely within the report. 

If you are aware of additional external scanners compatible with GitLab security reports please open an issue and contribute!
